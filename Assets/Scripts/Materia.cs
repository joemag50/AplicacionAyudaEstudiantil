﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;

public class Materia
{
    public int idMateria;
    public string Nombre;
    public string Maestro;
    public float CalFinal;

    public Materia(string nombre, string maestro, float calfinal)
    {
        this.Nombre = nombre.ToUpper();
        this.Maestro = maestro.ToUpper();
        this.CalFinal = calfinal;
    }

    public void SetCalFinal(float cal)
    {
        this.CalFinal = cal;
    }

    public string sqlInsert()
    {
        SiguienteIdMateriaDisponible();
        string sql = string.Format(" INSERT INTO Materias (IdMateria,Nombre, Maestro, CalFinal)" +
                                   " VALUES ({0}, '{1}', '{2}', {3})", this.idMateria, this.Nombre, this.Maestro, this.CalFinal);
        return sql;
    }

    public int SiguienteIdMateriaDisponible()
    {
        IDataReader reader;
        reader = BaseDatos.SqlCommand("SELECT COUNT(*) FROM Materias");
        if (reader.Read())
        {
            this.idMateria = reader.GetInt32(0);
            return this.idMateria;
        }
        return 0;
    }
}
