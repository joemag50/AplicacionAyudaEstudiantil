﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
//Importante para los GUI
using UnityEngine.UI;

public class ManagerConfInicial : MonoBehaviour
{
    public GameObject canvasPerfil, canvasMaterias,
                      inptNombre, inptMatricula,
                      inptNomMateria, inptMaestro, inptDias,
                      matutino, vespertino, nocturno,
                      inptInicio, inptDuracion;

    Perfil nuevoPerfil;
    List<Materia> materias;
    List<Horario> horario;

    private void Start()
    {
        materias = new List<Materia>();
        horario = new List<Horario>();
    }

    //Perfil
    public void Continuar()
    {
        string nom = inptNombre.GetComponent<InputField>().text;
        string mat = inptMatricula.GetComponent<InputField>().text;

        //Revisar si los campos estan vacios
         if (nom.Length == 0 || mat.Length == 0)
         {
            Utilerias.ShowPopUp("Campos Nombre y Matricula son obligatorios, Favor de completarlos", Vector3.zero);
            return;
         }

        //Guardar en una serializacion
        nuevoPerfil = new Perfil(nom, mat);
        
        //Ocultar este canvas
        canvasPerfil.SetActive(false);
        canvasMaterias.SetActive(true);
    }

    public void OtraMateria()
    {
        //Info materia
        string nom = inptNomMateria.GetComponent<InputField>().text;
        string maes = inptMaestro.GetComponent<InputField>().text;

        //Info horario
        int dias = inptDias.GetComponent<Dropdown>().value;
        int inic = int.Parse(inptInicio.GetComponent<Dropdown>().captionText.text);
        int dura = int.Parse(inptDuracion.GetComponent<Dropdown>().captionText.text);
        string turn = getToggleHorario();

        Debug.Log(dias+ " "+ inic + " " + dura);
        //Serie de validaciones
        if (nom.Length == 0 || maes.Length == 0)
        {
            Utilerias.ShowPopUp("Campos Nombre y Maestro son obligatorios, Favor de completarlos", Vector3.zero);
            return;
        }

        Materia m = new Materia(nom, maes, 0);
        bool existe = false;
        foreach (Materia mat in materias)
        {
            //No puede ser la misma materia, el profe puede que si
            if (Object.Equals(m.Nombre, mat.Nombre))
            {
                existe = true;
                break;
            }
        }
        if (existe)
        {
            Utilerias.ShowPopUp("Existe una materia igual, favor de dar de alta una diferente", Vector3.zero);
            return;
        }

        //Vamos a revisar que no se empalme la hora de inicio, la neta que hueva con la duracion
        Horario h = new Horario(m, dias, turn+inic, dura, "");
        existe = false;
        foreach (Horario hor in horario)
        {
            if (Object.Equals(h.HoraInicio, hor.HoraInicio) && Object.Equals(h.Dia, hor.Dia))
            {
                existe = true;
                break;
            }

            if (Object.Equals(h.HoraInicio, hor.HoraInicio) &&
                ((h.Dia == 0 && (hor.Dia == 1 || hor.Dia == 3 || hor.Dia == 5)) || (hor.Dia == 0 && (h.Dia == 1 || h.Dia == 3 || h.Dia == 5))))
            {
                existe = true;
                break;
            }
        }
        if (existe)
        {
            Utilerias.ShowPopUp("Existe un horario igual, favor de dar de alta uno diferente", Vector3.zero);
            return;
        }

        materias.Add(m);
        horario.Add(h);
        LimpiarMateria();

        Utilerias.ShowPopUp("Materia dada de alta correctamente", Vector3.zero);
    }

    public void Terminar()
    {
        //Damos de alta a la ultima materia
        OtraMateria();
        //Metemos primero el perfil
        BaseDatos.SqlCommand(nuevoPerfil.sqlInsert());
        //Luego damos de alta a las materias
        foreach (Materia mat in materias)
        {
            BaseDatos.SqlCommand(mat.sqlInsert());
        }

        foreach (Horario hor in horario)
        {
            BaseDatos.SqlCommand(hor.sqlInsert());
        }

        canvasPerfil.SetActive(false);
        canvasMaterias.SetActive(false);

        Utilerias.ShowPopUp("Configuracion terminada", Vector3.zero);

        SceneManager.LoadScene("Principal");
    }

    public string getToggleHorario()
    {
        if (matutino.GetComponent<Toggle>().isOn)
        {
            return matutino.name.Substring(0, 1);
        }

        if (vespertino.GetComponent<Toggle>().isOn)
        {
            return vespertino.name.Substring(0, 1);
        }

        if (nocturno.GetComponent<Toggle>().isOn)
        {
            return nocturno.name.Substring(0, 1);
        }

        return "";
    }

    public void LimpiarMateria()
    {
        inptNomMateria.GetComponent<InputField>().text = "";
        inptMaestro.GetComponent<InputField>().text = "";
    }
}
