﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenScale : MonoBehaviour
{
    Camera mainCamera;
    float touchesPrevPosDifference, touchesCurPosDifference, zoommodifier;
    Vector2 firstTouchPrevPos, secondTouchPrevPos;

    [SerializeField]
    float zoomModifiersspeed = 0.1f;

	// Use this for initialization
	void Start ()
    {
        mainCamera = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.touchCount == 2)
        {
            
            Touch firstTouch = Input.GetTouch(0);
            Touch secondTouch = Input.GetTouch(1);

            firstTouchPrevPos = firstTouch.position - firstTouch.deltaPosition;
            secondTouchPrevPos = secondTouch.position - secondTouch.deltaPosition;

            touchesPrevPosDifference = (firstTouchPrevPos - secondTouchPrevPos).magnitude;
            touchesCurPosDifference = (firstTouch.position - secondTouch.position).magnitude;

            zoommodifier = (firstTouch.deltaPosition - secondTouch.deltaPosition).magnitude * zoomModifiersspeed;

            if (touchesPrevPosDifference > touchesCurPosDifference)
                mainCamera.orthographicSize += zoommodifier;
            if (touchesPrevPosDifference < touchesCurPosDifference)
                mainCamera.orthographicSize -= zoommodifier;

            mainCamera.orthographicSize = Mathf.Clamp(mainCamera.orthographicSize, 5f, 22f);
        }
    }
}
