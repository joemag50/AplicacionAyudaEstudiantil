﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Horario
{
    public Materia Materia;
    public int Dia;
    public string HoraInicio;
    public int Duracion;
    public string Salon;

    public Horario(Materia materia, int dia, string horainicio, int duracion, string salon)
    {
        this.Materia = materia;
        this.Dia = dia;
        this.HoraInicio = horainicio.ToUpper();
        this.Duracion = duracion;
        this.Salon = salon;
    }

    public string sqlInsert()
    {
        string sql = string.Format(" INSERT INTO horario (idMateria,dia,horainicio,duracion,salon)" +
                                   " VALUES ({0},{1},'{2}',{3},'{4}')", Materia.idMateria, Dia, HoraInicio, Duracion, Salon);
        return sql;
    }
}
