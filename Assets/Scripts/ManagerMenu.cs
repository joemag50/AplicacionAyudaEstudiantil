﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Mono.Data.Sqlite;
using System.Data;
using System;

public class ManagerMenu : MonoBehaviour
{
    public Canvas[] AreasTrabajo;
    public Canvas AreaPrincipal;
    IDataReader reader;

    // Use this for initialization
    void Start ()
    {
        reader = BaseDatos.SqlCommand("SELECT * FROM Perfil");
        if (reader.Read())
        {
            Utilerias.ShowPopUp("Usted es " + reader.GetString(0) + " " + reader.GetString(1), Vector3.zero);
        }
    }

    // Update is called once per frame
    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            MenuRegresar();
        }
    }

    public void MenuHorario()
    {
        MostrarCanvas(0);
    }

    public void MenuNotas()
    {
        MostrarCanvas(1);
    }

    public void MenuAsistencias()
    {
        MostrarCanvas(2);
    }

    public void MenuPromedio()
    {
        MostrarCanvas(3);
    }

    //Con este vamos a regresar al menu principal
    public void MenuRegresar()
    {
        MostrarCanvas(-1);
    }

    //0 - Horario
    //1 - Notas
    //2 - Asistencia
    //4 - Promedio
    private void MostrarCanvas(int canvas)
    {
        if (canvas == -1)
            AreaPrincipal.gameObject.SetActive(true);
        else
            AreaPrincipal.gameObject.SetActive(false);

        for (int i = 0; i < AreasTrabajo.Length; i++)
        {
            if (i == canvas)
            {
                AreasTrabajo[i].gameObject.SetActive(true);
            }
            else
            {
                AreasTrabajo[i].gameObject.SetActive(false);
            }
        }
    }
}
