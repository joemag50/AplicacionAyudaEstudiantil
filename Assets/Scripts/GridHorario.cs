﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;
using Mono.Data.Sqlite;
using System.Data;
using System;
public class GridHorario : MonoBehaviour
{
    public GameObject Horas, Lunes, Martes, Miercoles, Jueves, Viernes, Sabado;
    public TextMeshProUGUI[] horas, lunes, martes, miercoles, jueves, viernes, sabado;

    List<string> horasHack;
	// Use this for initialization
	void Start ()
    {
        RellenaHoras();
        RellenaLunes();
        RellenaMartes();
        RellenaMiercoles();
        RellenaJueves();
        RellenaViernes();
        RellenaSabado();
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void RellenaHoras()
    {
        horas = Horas.GetComponentsInChildren<TextMeshProUGUI>();
        int i = 0;
        int j = 1;
        horasHack = new List<string>();
        foreach (TextMeshProUGUI text in horas)
        {
            string titulo = "";
            if (i <= 5)
                titulo = "M";

            if (i > 5 && i <= 11)
            {
                if (i == 6)
                    j = 1;

                titulo = "V";
            }


            if (i > 11)
            {
                if (i == 12)
                    j = 1;

                titulo = "N";
            }
            horasHack.Add(titulo + j);
            text.SetText(titulo+j);
            j++;
            i++;
        }
    }

    private void RellenaLunes()
    {
        lunes = Lunes.GetComponentsInChildren<TextMeshProUGUI>();
        IDataReader reader;
        reader = BaseDatos.SqlCommand("SELECT nombre, horainicio, duracion FROM Horario LEFT JOIN Materias USING (idmateria) WHERE dia = 0 OR dia = 1");
        //Primero los guardamos en un temp
        List<string> materias = new List<string>();
        List<string> horainicios = new List<string>();
        List<int> duraciones = new List<int>();

        while (reader.Read())
        {
            materias.Add(reader.GetString(0));
            horainicios.Add(reader.GetString(1));
            duraciones.Add(reader.GetInt32(2));
        }

        int i = 0;
        foreach (string s in horasHack)
        {
            lunes[horasHack.IndexOf(s)].SetText("");

            if (!(horainicios.Count <= i))
            {
                if (System.Object.Equals(s, horainicios[i]))
                {
                    lunes[horasHack.IndexOf(s)].SetText(materias[i]);
                    i++;
                }
            }
        }
    }

    private void RellenaMartes()
    {
        martes = Martes.GetComponentsInChildren<TextMeshProUGUI>();
        IDataReader reader;
        reader = BaseDatos.SqlCommand("SELECT nombre, horainicio, duracion FROM Horario LEFT JOIN Materias USING (idmateria) WHERE dia = 2");
        //Primero los guardamos en un temp
        List<string> materias = new List<string>();
        List<string> horainicios = new List<string>();
        List<int> duraciones = new List<int>();

        while (reader.Read())
        {
            materias.Add(reader.GetString(0));
            horainicios.Add(reader.GetString(1));
            duraciones.Add(reader.GetInt32(2));
        }

        int i = 0;
        foreach (string s in horasHack)
        {
            martes[horasHack.IndexOf(s)].SetText("");

            if (!(horainicios.Count <= i))
            {
                if (System.Object.Equals(s, horainicios[i]))
                {
                    martes[horasHack.IndexOf(s)].SetText(materias[i]);
                    i++;
                }
            }
        }
    }

    private void RellenaMiercoles()
    {
        miercoles = Miercoles.GetComponentsInChildren<TextMeshProUGUI>();
        IDataReader reader;
        reader = BaseDatos.SqlCommand("SELECT nombre, horainicio, duracion FROM Horario LEFT JOIN Materias USING (idmateria) WHERE dia = 0 OR dia = 3");
        //Primero los guardamos en un temp
        List<string> materias = new List<string>();
        List<string> horainicios = new List<string>();
        List<int> duraciones = new List<int>();

        while (reader.Read())
        {
            materias.Add(reader.GetString(0));
            horainicios.Add(reader.GetString(1));
            duraciones.Add(reader.GetInt32(2));
        }

        int i = 0;
        foreach (string s in horasHack)
        {
            miercoles[horasHack.IndexOf(s)].SetText("");

            if (!(horainicios.Count <= i))
            {
                if (System.Object.Equals(s, horainicios[i]))
                {
                    miercoles[horasHack.IndexOf(s)].SetText(materias[i]);
                    i++;
                }
            }
        }
    }

    private void RellenaJueves()
    {
        jueves = Jueves.GetComponentsInChildren<TextMeshProUGUI>();
        IDataReader reader;
        reader = BaseDatos.SqlCommand("SELECT nombre, horainicio, duracion FROM Horario LEFT JOIN Materias USING (idmateria) WHERE dia = 4");
        //Primero los guardamos en un temp
        List<string> materias = new List<string>();
        List<string> horainicios = new List<string>();
        List<int> duraciones = new List<int>();

        while (reader.Read())
        {
            materias.Add(reader.GetString(0));
            horainicios.Add(reader.GetString(1));
            duraciones.Add(reader.GetInt32(2));
        }

        int i = 0;
        foreach (string s in horasHack)
        {
            jueves[horasHack.IndexOf(s)].SetText("");

            if (!(horainicios.Count <= i))
            {
                if (System.Object.Equals(s, horainicios[i]))
                {
                    jueves[horasHack.IndexOf(s)].SetText(materias[i]);
                    i++;
                }
            }
        }
    }

    private void RellenaViernes()
    {
        viernes = Viernes.GetComponentsInChildren<TextMeshProUGUI>();
        IDataReader reader;
        reader = BaseDatos.SqlCommand("SELECT nombre, horainicio, duracion FROM Horario LEFT JOIN Materias USING (idmateria) WHERE dia = 0 OR dia = 5");
        //Primero los guardamos en un temp
        List<string> materias = new List<string>();
        List<string> horainicios = new List<string>();
        List<int> duraciones = new List<int>();

        while (reader.Read())
        {
            materias.Add(reader.GetString(0));
            horainicios.Add(reader.GetString(1));
            duraciones.Add(reader.GetInt32(2));
        }

        int i = 0;
        foreach (string s in horasHack)
        {
            viernes[horasHack.IndexOf(s)].SetText("");

            if (!(horainicios.Count <= i))
            {
                if (System.Object.Equals(s, horainicios[i]))
                {
                    viernes[horasHack.IndexOf(s)].SetText(materias[i]);
                    i++;
                }
            }
        }
    }

    private void RellenaSabado()
    {
        sabado = Sabado.GetComponentsInChildren<TextMeshProUGUI>();
        IDataReader reader;
        reader = BaseDatos.SqlCommand("SELECT nombre, horainicio, duracion FROM Horario LEFT JOIN Materias USING (idmateria) WHERE dia = 6");
        //Primero los guardamos en un temp
        List<string> materias = new List<string>();
        List<string> horainicios = new List<string>();
        List<int> duraciones = new List<int>();

        while (reader.Read())
        {
            materias.Add(reader.GetString(0));
            horainicios.Add(reader.GetString(1));
            duraciones.Add(reader.GetInt32(2));
        }

        int i = 0;
        foreach (string s in horasHack)
        {
            sabado[horasHack.IndexOf(s)].SetText("");

            if (!(horainicios.Count <= i))
            {
                if (System.Object.Equals(s, horainicios[i]))
                {
                    sabado[horasHack.IndexOf(s)].SetText(materias[i]);
                    i++;
                }
            }
        }
    }
}
