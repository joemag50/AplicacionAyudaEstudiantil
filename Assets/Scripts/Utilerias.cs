﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utilerias : MonoBehaviour
{
    public static void ShowPopUp(string text, Vector3 pos)
    {
        GameObject popup = (GameObject)Resources.Load("popUpCanvas");
        GameObject p = Instantiate(popup, pos, Quaternion.identity);
        p.GetComponent<popUpCanvas>().ShowMessage(text);
    }
}
