﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using Mono.Data.Sqlite;
using System.Data;
using System;
public class ManagerPrincipal : MonoBehaviour
{
    IDataReader reader;

    //Vamos a la Escena donde rellenamos los datos principales
    public void IniciarConfiguracion()
    {
        SceneManager.LoadScene("ConfInicial");
    }

    //Una vez que los datos esten listos vamos directo a la app
    //TODO: Puede ser automatico???
    public void Iniciar()
    {
        Debug.Log("Boton Iniciar");
        reader = BaseDatos.SqlCommand("SELECT * FROM Perfil");
        if (!reader.Read())
        {
            IniciarConfiguracion();
        }
        else
        {
            SceneManager.LoadScene("Menu");
        }
    }

    public void borrarDBPrueba()
    {
        IDataReader reader1 = BaseDatos.SqlCommand("DELETE FROM Perfil");
        IDataReader reader2 = BaseDatos.SqlCommand("DELETE FROM Materias");
        IDataReader reader3 = BaseDatos.SqlCommand("DELETE FROM Horario");
    }
}
