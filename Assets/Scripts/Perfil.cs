﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perfil
{
    public string Nombre;
    public string Matricula;

    public Perfil(string nom, string mat)
    {
        this.Nombre = nom;
        this.Matricula = mat;
    }

    public string sqlInsert()
    {
        string sql = string.Format("INSERT INTO Perfil VALUES ('{0}', '{1}')",this.Nombre, this.Matricula);
        return sql;
    }
}
