﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class popUpCanvas : MonoBehaviour
{
    float timeLeft = 3;
    public void ShowMessage(string text)
    {
        GetComponentInChildren<Text>().text = text;
    }

    private void Update()
    {
        timeLeft -= Time.deltaTime;
        if (timeLeft < 0)
        {
            Destroy(GameObject.FindGameObjectWithTag("Mensaje"));
        }
    }
}
